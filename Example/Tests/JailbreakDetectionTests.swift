import UIKit
import XCTest
import Swift_Json

@testable import RadixLib

class JailbreakDetectionTests: XCTestCase, JailbreakDetectionDelegate {
    let detectExpectationIgnoreSimulator = XCTestExpectation(description: "Detect Expectation Ignore Simulator")
    let detectExpectationNotIgnoreSimulator = XCTestExpectation(description: "Detect Expectation Not Ignore Simulator")
    private var jailBreakDetected: Bool? = nil
    
    func jailbreakDetected() {
        self.jailBreakDetected = true
        self.detectExpectationNotIgnoreSimulator.fulfill()
    }
    
    func jailbreakNotDetected() {
        self.jailBreakDetected = false
        self.detectExpectationIgnoreSimulator.fulfill()
    }
    
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test(){
        
        JailbreakDetection(delegate: self).detect()
        
        self.wait(for: [detectExpectationIgnoreSimulator], timeout: 0.5)
        
        assert(jailBreakDetected == false)
        
        JailbreakDetection(delegate: self).detect(detectInSimulator: true)
        
        
        self.wait(for: [detectExpectationNotIgnoreSimulator], timeout: 0.5)
        
        assert(jailBreakDetected == true)
    }
}
