//
//  StorageTests.swift
//  RadixLib
//
//  Created by Anderson Lucas C. Ramos on 14/06/17.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import Foundation
import XCTest
import Swift_Json
@testable import RadixLib

@objc(Hospitalization)
class Hospitalization: NSObject {
    @objc dynamic var previousHospitalizations: [NSObject]?
    @objc dynamic var id: Int = 0
    @objc dynamic var patientName: String?
    @objc dynamic var patientBornDate: String?
    @objc dynamic var patientHeight: Double = 0.0
    @objc dynamic var patientWeight: Int = 0
    @objc dynamic var admissionDate: String?
    @objc dynamic var exitDate: String?
    @objc dynamic var death: Bool = false
    @objc dynamic var barCode: String?
    @objc dynamic var locationType: String?
    @objc dynamic var locationSession: String?
    @objc dynamic var locationBed: String?
    @objc dynamic var attendanceType: String?
    @objc dynamic var hospitalizationType: String?
    @objc dynamic var medicalRecordsNumber: String?
    @objc dynamic var mainProcedureTUSSId: String?
    @objc dynamic var mainProcedureTUSSDisplayName: String?
    @objc dynamic var mainProcedureCRM: String?
    @objc dynamic var diagnosticHypothesisList: [NSObject]?
    @objc dynamic var secondaryCIDList: [NSObject]?
    @objc dynamic var observationList: [NSObject]?
    @objc dynamic var examRequestList: [NSObject]?
    @objc dynamic var furtherOpinionList: [NSObject]?
    @objc dynamic var medicalProceduresList: [NSObject]?
    @objc dynamic var medicineUsageList: [NSObject]?
    @objc dynamic var timeDependentMedicineUsageList: [NSObject]?
    @objc dynamic var clinicalIndication: String?
    @objc dynamic var medicineReintegration: String?
    @objc dynamic var welcomeHomeIndication: String?
    @objc dynamic var lastLocalModification: String?
    @objc dynamic var removedItemList: String?
}

@objc(Hospital)
class Hospital: NSObject {
    @objc dynamic var id: Int = 0
    @objc dynamic var hospitalizationList: [Hospitalization]?
}

class Content: NSObject {
    @objc dynamic var hospitalList: [Hospital]?
//    dynamic var mailboxes: [Mailbox]?
}

class Response: NSObject {
    @objc dynamic var content: Content?
    @objc dynamic var responseError: String?
    @objc dynamic var success: Bool = false
}

class StorageTests : XCTestCase, StorageProtocol {
    let testUrl: String = "http://www.mocky.io/v2/591e0c273f0000b80b77c939"
    
    let requestExpectation = XCTestExpectation(description: "Request Expectation")
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testStandaloneBaseRequest() {
        class Delegate: BaseRequestProtocol {
            fileprivate(set) var response: Response?
            
            weak var test: StorageTests?
            
            func requestDidFinish(withSuccess response: BaseRequestResponse) {
                if response.statusCode == 200 {
                    let parser = JsonParser()
                    guard let response: Response = parser.parse(data: response.responseData!) else {
                        self.requestDidFinish(withError: .noResponseData)
                        return
                    }
                    self.response = response
                    self.test?.requestExpectation.fulfill()
                } else {
                    self.requestDidFinish(withError: .statusCode(code: response.statusCode, data: response.responseData))
                }
            }
            
            func requestDidFinish(withError error: BaseRequestError) {
                assertionFailure("Couldn't execute request...")
            }
        }
        
        let delegate = Delegate()
        delegate.test = self
        
        let base = BaseRequest(with: testUrl, and: .get)
        base.delegate = delegate
        
        base.fetch()
        
        self.wait(for: [self.requestExpectation], timeout: 5.0)
        
        assert(delegate.response != nil)
    }
    
    func testPinnedCertificated() {
        class Delegate: BaseRequestProtocol {
            fileprivate(set) var response: Response?
            fileprivate(set) var success: Bool?
            
            weak var test: StorageTests?
            
            func requestDidFinish(withSuccess response: BaseRequestResponse) {
                if response.statusCode == 200 {
                    self.success = true
                    self.test?.requestExpectation.fulfill()
                } else {
                    self.requestDidFinish(withError: .statusCode(code: response.statusCode, data: response.responseData))
                }
            }
            
            func requestDidFinish(withError error: BaseRequestError) {
                self.test?.requestExpectation.fulfill()
            }
        }
        
        let delegate = Delegate()
        delegate.test = self
        
        let base = BaseRequest(with: "https://www.ssllabs.com/", and: .get, pinningCertificateFile: Bundle(for: type(of: self)).path(forResource: "cert", ofType: "der"))
        base.delegate = delegate
        
        base.fetch()
        
        self.wait(for: [self.requestExpectation], timeout: 5.0)
        XCTAssert(delegate.success ?? false)
    }
    
    func testFailPinnedCertificated() {
        class Delegate: BaseRequestProtocol {
            fileprivate(set) var response: Response?
            fileprivate(set) var success: Bool?
            
            weak var test: StorageTests?
            
            func requestDidFinish(withSuccess response: BaseRequestResponse) {
                self.test?.requestExpectation.fulfill()
            }
            
            func requestDidFinish(withError error: BaseRequestError) {
                self.success = true
                self.test?.requestExpectation.fulfill()
            }
        }
        
        let delegate = Delegate()
        delegate.test = self
        
        let base = BaseRequest(with: "https://www.ssllabs.com/", and: .get, pinningCertificateFile: Bundle(for: type(of: self)).path(forResource: "failCert", ofType: "der"))
        base.delegate = delegate
        
        base.fetch()
        
        self.wait(for: [self.requestExpectation], timeout: 5.0)
        XCTAssert(delegate.success ?? false)
    }
    
    func testStorageAutomaticModelProviding() {
        class Provider : StorageContentProviderProtocol {
            func objectModel(for info: Dictionary<String, Any>) -> AnyObject? {
                guard let statusCode = info["statusCode"] as? Int, statusCode == 200 else {
                    return nil
                }
                let data: Data? = info["responseData"] as! Data?
                
                let parser = JsonParser()
                let response: Response? = parser.parse(data: data!)
                return response
            }
            
            func data(for objectMode: Any) -> Data? {
                return nil
            }
        }
        
        let request = BaseRequest(with: testUrl, and: .get)
        
        let storage = Storage()
        storage.delegate = self
        try! storage.fetch(from: request, with: Provider())
        
        self.wait(for: [self.requestExpectation], timeout: 5.0)
    }
    
    // MARK: StorageResultProtocol implementations
    
    func storage(_ storage: Storage, didFinishWith result: Any) {
        assert(result is Response)
        
        self.requestExpectation.fulfill()
    }
    
    func storage<ErrorType>(_ storage: Storage, didFinishWith error: ErrorType) {
        assertionFailure("Couldn't execute request... \(error)")
    }
}
