//
//  ImageProcessing.swift
//  Pods
//
//  Created by Anderson Lucas C. Ramos on 16/06/17.
//
//

import UIKit

public extension UIImage {
    public func image(scaledBy baseWidth: CGFloat) -> UIImage? {
        let currentSize = self.size
        let tall = currentSize.width < currentSize.height
        
        let proportion = tall ? (currentSize.width / currentSize.height) : (currentSize.height / currentSize.width)
        
        var newSize: CGSize
        if tall {
            newSize = CGSize(width: baseWidth, height: baseWidth / proportion)
        } else {
            newSize = CGSize(width: baseWidth / proportion, height: baseWidth)
        }
        return self.image(with: newSize)
    }
    
    public func image(with newSize: CGSize) -> UIImage? {
        UIGraphicsBeginImageContext(newSize)
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        UIGraphicsGetCurrentContext()?.flush()
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
}
