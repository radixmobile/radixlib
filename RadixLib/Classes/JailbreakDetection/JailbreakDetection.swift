//
//  JailbreakDetection.swift
//  RadixLib
//
//  Created by Julio Cesar da Silva Pereira on 23/10/18.
//

import Foundation

public class JailbreakDetection {
    public weak var delegate: JailbreakDetectionDelegate?
    
    public init(delegate: JailbreakDetectionDelegate? = nil) {
        self.delegate = delegate
    }
    
    public func detect(detectInSimulator: Bool = false){
        if self.isJailbroken(detectInSimulator: detectInSimulator) {
            self.delegate?.jailbreakDetected()
        } else {
            self.delegate?.jailbreakNotDetected()
        }
    }
    
    private func isJailbroken(detectInSimulator: Bool = false) -> Bool {
        if TARGET_OS_SIMULATOR != 1 || detectInSimulator {
            // Check 1 : existence of files that are common for jailbroken devices
            if FileManager.default.fileExists(atPath: "/Applications/Cydia.app")
                || FileManager.default.fileExists(atPath: "/Library/MobileSubstrate/MobileSubstrate.dylib")
                || FileManager.default.fileExists(atPath: "/bin/bash")
                || FileManager.default.fileExists(atPath: "/usr/sbin/sshd")
                || FileManager.default.fileExists(atPath: "/etc/apt")
                || FileManager.default.fileExists(atPath: "/private/var/lib/apt/")
                || UIApplication.shared.canOpenURL(URL(string:"cydia://package/com.example.package")!) {
                return true
            }
            
            // Check 2 : Reading and writing in system directories (sandbox violation)
            let stringToWrite = "Jailbreak Test"
            do {
                try stringToWrite.write(toFile: "/private/JailbreakTest.txt",
                                        atomically: true,
                                        encoding: String.Encoding.utf8)
                //Device is jailbroken
                return true
            } catch {
                return false
            }
        } else {
            return false
        }
    }
}
