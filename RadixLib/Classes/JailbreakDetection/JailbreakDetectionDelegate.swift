//
//  JailbreakDetectionDelegate.swift
//  RadixLib
//
//  Created by Julio Cesar da Silva Pereira on 23/10/18.
//

import Foundation

public protocol JailbreakDetectionDelegate: class {
    func jailbreakDetected()
    func jailbreakNotDetected()
}
