//
//  Logger.swift
//  RadixLib
//
//  Created by Anderson Lucas C. Ramos on 07/02/17.
//  Copyright © 2017 Radix Eng & Soft. All rights reserved.
//

import Foundation

public protocol Logger {
	func log(_ string: String)
}

public extension Logger {
	public func log(_ string: String) {
    #if DEBUG
        let classWithPackage = "\(self)"
        let comps = classWithPackage.components(separatedBy: ".")
        var className = classWithPackage
        if comps.count > 0 {
            className = comps[1]
        }
		print("[\(className.uppercased())] \(string)")
    #endif
	}
}
