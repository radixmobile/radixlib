//
//  BaseRequest.swift
//  RadixLib
//
//  Created by Anderson Lucas C. Ramos on 06/02/17.
//  Copyright © 2017 Radix Eng & Soft. All rights reserved.
//

import Foundation
import Swift_Json

public enum BaseRequestMethod {
    case get, post, put, delete
}

open class BaseRequest : Logger, StorageProviderProtocol {
    fileprivate var sessionTask: URLSessionDataTask!
    fileprivate let requestId = UUID().uuidString.hashValue
    fileprivate var requestConfig: (url: String, method: BaseRequestMethod)!
    
    open fileprivate(set) var session: URLSession!
    open fileprivate(set) var isRunning: Bool = false
    open fileprivate(set) var isCancelable: Bool = true
    
    open weak var delegate: BaseRequestProtocol?
    
    open var headerFields: [String: String]?
    open var queryParams: [String: String]?
    open var stringBodyContent: String?
    open var jsonBodyObject: AnyObject?
    open var jsonConfig: JsonConfig?
    open var shouldLogRequest: Bool = true
    
    open var sessionDelegate: URLSessionDelegate?
    
    public required convenience init() throws {
        throw NSError(domain: "You MUST override this init and call self.init(with:and:)", code: -1, userInfo: nil)
    }
    
    public init(with url: String, and method: BaseRequestMethod, sessionConfiguration: URLSessionConfiguration = URLSessionConfiguration.default, pinningCertificateFile: String? = nil) {
        if let pinningCertificateFile = pinningCertificateFile {
            self.sessionDelegate = NSURLSessionPinningDelegate(file_der: pinningCertificateFile)
            self.session = URLSession(configuration: sessionConfiguration,
                                      delegate: self.sessionDelegate,
                                      delegateQueue: nil)
        } else {
            self.session = URLSession(configuration: sessionConfiguration)
        }
        self.requestConfig = (url: url, method: method)
    }
    
    public func fetch() {
        if let config = self.requestConfig, !config.url.isEmpty {
            switch config.method {
            case .get: self.get(urlString: config.url); break
            default:
                self.delegate?.requestDidFinish(withError: .wrongRequestMethod(configuredMethod: config.method))
                break
            }
        } else {
            self.dispatchError(.unsuppliedUrl)
        }
    }
    
    public func retain() {
        if let config = self.requestConfig, !config.url.isEmpty {
            switch config.method {
            case .post: self.post(urlString: config.url); break
            default:
                self.delegate?.requestDidFinish(withError: .wrongRequestMethod(configuredMethod: config.method))
                break
            }
        } else {
            self.dispatchError(.unsuppliedUrl)
        }
    }
    
    public func update() {
        if let config = self.requestConfig, !config.url.isEmpty {
            switch config.method {
            case .put: self.put(urlString: config.url); break
            default:
                self.delegate?.requestDidFinish(withError: .wrongRequestMethod(configuredMethod: config.method))
                break
            }
        } else {
            self.dispatchError(.unsuppliedUrl)
        }
    }
    
    public func delete() {
        if let config = self.requestConfig, !config.url.isEmpty {
            switch config.method {
            case .delete: self.delete(urlString: config.url); break
            default:
                self.delegate?.requestDidFinish(withError: .wrongRequestMethod(configuredMethod: config.method))
                break
            }
        } else {
            self.dispatchError(.unsuppliedUrl)
        }
    }
    
    public func cancel() {
        self.cancelRequest()
    }
    
    open func getHeaderFields() -> [String: String]? {
        return self.headerFields
    }
    
    open func getQueryParams() -> [String: String]? {
        return self.queryParams
    }
    
    open func getStringBodyContent() -> String? {
        return self.stringBodyContent
    }
    
    open func getJsonBodyObject<T : NSObject>() -> T? {
        return self.jsonBodyObject as? T
    }
    
    open func getJsonConfig() -> JsonConfig? {
        return self.jsonConfig
    }
    
    fileprivate func requestLog(_ content: String) {
        if self.shouldLogRequest {
            self.log("[REQ ID: \(self.requestId)] - \(content)")
        }
    }
    
    fileprivate func setupHeaders(_ request: NSMutableURLRequest, _ headers: [String: String]) {
        for header in headers {
            request.addValue(header.value, forHTTPHeaderField: header.key)
        }
        if let headers = request.allHTTPHeaderFields {
            self.requestLog("Request Headers: \(headers)")
        }
    }
    
    fileprivate func setupQueryParams(_ url: String, _ params: [String: String]) -> String {
        if params.count == 0 { return "" } // grant that there is at least one parameter
        
        var queryString = url.contains("?") ? "&" : "?"
        for param in params {
            guard let scapedKey = param.key.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else { continue }
            guard let scapedValue = param.value.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else { continue }
            queryString += "\(scapedKey)=\(scapedValue)&"
        }
        queryString.remove(at: queryString.index(before: queryString.endIndex))
        return queryString
    }
    
    fileprivate func setupStreamBodyParams(_ request: NSMutableURLRequest, _ params: [String: String]) {
        var stringParams = self.setupQueryParams("?", params)
        stringParams.remove(at: stringParams.index(stringParams.startIndex, offsetBy: 0))
        self.requestLog("------ Request Body BEGIN")
        self.requestLog("Request Body: \(stringParams)")
        self.requestLog("------ Request Body END")
        request.httpBody = stringParams.data(using: .utf8)
    }
    
    fileprivate func setupStreamStringBody(_ request: NSMutableURLRequest, _ content: String) {
        self.requestLog("------ Request Body BEGIN")
        self.requestLog("Request Body: \(content)")
        self.requestLog("------ Request Body END")
        request.httpBody = content.data(using: .utf8)
    }
    
    fileprivate func setupStreamJsonBody(_ request: NSMutableURLRequest, _ object: NSObject) {
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let writer = JsonWriter()
        guard let jsonBodyData: Data = writer.write(anyObject: object, withConfig: self.getJsonConfig()) else {
            self.requestLog("Could not set the URLRequest.httpBody, the JsonWriter returned nil.")
            return
        }
        self.requestLog("------ Request Body BEGIN")
        let logString = String(data: jsonBodyData, encoding: .utf8)
        self.requestLog("Request Body: \(logString ?? "[No body data...]")")
        self.requestLog("------ Request Body END")
        request.httpBody = jsonBodyData
    }
    
    fileprivate func generateQueryRequest(_ request: NSMutableURLRequest) -> NSMutableURLRequest {
        self.requestLog("Request HttpMethod: \(request.httpMethod)")
        guard let params = self.getQueryParams() else {
            self.requestLog("Request URL: \(request.url?.absoluteString ?? "")")
            return request
        }
        guard var urlString = request.url?.absoluteString else {
            return request
        }
        urlString += self.setupQueryParams(urlString, params)
        request.url = URL(string: urlString)
        let path = urlString.components(separatedBy: "?")
        if path.count > 1 {
            self.requestLog("Request URL: \(path[0])")
            self.requestLog("Request Query: \(path[1])")
        } else {
            self.requestLog("Request URL: \(urlString)")
        }
        return request
    }
    
    fileprivate func generateStreamRequest(_ request: NSMutableURLRequest) -> NSMutableURLRequest {
        self.requestLog("Request HttpMethod \(request.httpMethod)")
        guard let params = self.getQueryParams() else {
            guard let bodyObject = self.getJsonBodyObject() else {
                guard let stringContent = self.getStringBodyContent() else {
                    return request
                }
                self.setupStreamStringBody(request, stringContent)
                return request
            }
            self.setupStreamJsonBody(request, bodyObject)
            return request
        }
        self.setupStreamBodyParams(request, params)
        return request
    }
    
    fileprivate func generate(request: NSMutableURLRequest, method: BaseRequestMethod) -> NSMutableURLRequest {
        switch method {
        case .get:
            request.httpMethod = "GET"
            return self.generateQueryRequest(request)
        case .post:
            request.httpMethod = "POST"
            return self.generateStreamRequest(request)
        case .put:
            request.httpMethod = "PUT"
            return self.generateStreamRequest(request)
        case .delete:
            request.httpMethod = "DELETE"
            return self.generateStreamRequest(request)
        }
    }
    
    fileprivate func generateRequest(url: URL, method: BaseRequestMethod) -> URLRequest {
        self.requestLog("------ BEGIN OF REQUEST")
        
        let request = NSMutableURLRequest(url: url)
        guard let headers = self.getHeaderFields() else {
            return self.generate(request: request, method: method) as URLRequest
        }
        self.setupHeaders(request, headers)
        return self.generate(request: request, method: method) as URLRequest
    }
    
    fileprivate func startSession(forRequest request: URLRequest) {
        self.sessionTask = self.session.dataTask(with: request) { [weak self] (data, response, error) in
            self?.processDataTask(data, response, error)
        }
        self.sessionTask.resume()
    }
    
    fileprivate func doGet(urlString: String) {
        guard let url = URL(string: urlString) else { return }
        let request = self.generateRequest(url: url, method: .get)
        self.startSession(forRequest: request)
    }
    
    fileprivate func doPost(urlString: String) {
        guard let url = URL(string: urlString) else { return }
        let request = self.generateRequest(url: url, method: .post)
        self.startSession(forRequest: request)
    }
    
    fileprivate func doPut(urlString: String) {
        guard let url = URL(string: urlString) else { return }
        let request = self.generateRequest(url: url, method: .put)
        self.startSession(forRequest: request)
    }
    
    fileprivate func doDelete(urlString: String) {
        guard let url = URL(string: urlString) else { return }
        let request = self.generateRequest(url: url, method: .delete)
        self.startSession(forRequest: request)
    }
    
    fileprivate func processDataTask(_ responseData: Data?, _ urlResponse: URLResponse?, _ responseError: Error?) {
        let result = RequestResult()
        result.data = responseData
        result.response = urlResponse as? HTTPURLResponse
        result.error = responseError
        self.requestDidFinish(withResult: result)
    }
    
    fileprivate func requestDidFinish(withResult result: RequestResult) {
        self.isRunning = false
        self.sessionTask = nil
        
        if result.error == nil {
            if result.response != nil && result.response!.statusCode > 299 {
                self.dispatchError(.statusCode(code: result.response!.statusCode, data: result.data))
                return
            }
            
            guard let data = result.data else {
                self.dispatchError(.noResponseData)
                return
            }
            
            self.dispatchSuccess(result.response!.statusCode, data)
        } else {
            self.dispatchError(.connection(error: result.error!))
        }
    }
    
    fileprivate func dispatchError(_ error: BaseRequestError) {
        self.requestLog("Request result: \(error)")
        
        self.requestDidFinish(withError: error)
        
        self.requestLog("------ END OF REQUEST")
    }
    
    fileprivate func dispatchSuccess(_ statusCode: Int, _ data: Data) {
        self.requestLog("Request result: statusCode - \(statusCode), content: \(String(data: data, encoding: .utf8) ?? "[NOT A STRING CONTENT]")")
        
        self.requestDidFinish(withStatusCode: statusCode, andData: data)
        
        self.requestLog("------ END OF REQUEST")
    }
    
    public func getBaseErrorString(_ error: BaseRequestError) -> String {
        switch error {
        case .statusCode(let code, let data):
            let dataString = data != nil ? String(data: data!, encoding: .utf8) : nil
            if dataString == nil {
                return "status code \(code)"
            }
            return "status code \(code), response: \(dataString!)"
        default:
            return "\(error)"
        }
    }
    
    open func requestDidFinish(withStatusCode statusCode: Int, andData data: Data) {
        var response = BaseRequestResponse()
        response.statusCode = statusCode
        response.responseData = data
        self.delegate?.requestDidFinish(withSuccess: response)
    }
    
    open func requestDidFinish(withError error: BaseRequestError) {
        self.delegate?.requestDidFinish(withError: error)
    }
}

internal class RequestResult : NSObject {
    fileprivate(set) var data: Data?
    fileprivate(set) var response: HTTPURLResponse?
    fileprivate(set) var error: Error?
}

internal extension BaseRequest {
    func get(urlString: String) {
        if !self.isRunning {
            self.isRunning = true
            self.doGet(urlString: urlString)
        }
    }
    
    func post(urlString: String) {
        if !self.isRunning {
            self.isRunning = true
            self.doPost(urlString: urlString)
        }
    }
    
    func put(urlString: String) {
        if !self.isRunning {
            self.isRunning = true
            self.doPut(urlString: urlString)
        }
    }
    
    func delete(urlString: String) {
        if !self.isRunning {
            self.isRunning = true
            self.doDelete(urlString: urlString)
        }
    }
    
    func cancelRequest() {
        if self.isRunning && self.sessionTask != nil {
            self.isRunning = false
            self.sessionTask.cancel()
            self.sessionTask = nil
        }
    }
}

class NSURLSessionPinningDelegate: NSObject, URLSessionDelegate {
    
    fileprivate var xp: String
    
    init(file_der: String) {
        self.xp = file_der
    }
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Swift.Void) {
        
        // get the public key offered by the server
        if let serverTrust = challenge.protectionSpace.serverTrust ,let actualKey = SecTrustCopyPublicKey(serverTrust), let certData = NSData(contentsOfFile: self.xp) {
            
            // extract the expected public key
            let expectedKey = self.importPublicKeyReferenceFromDERCertificate(certData: certData)
            
            // check a match
            if let expectedKey = expectedKey, actualKey == expectedKey {
                completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust:serverTrust))
            } else {
                // Pinning failed
                completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
            }
        } else {
            // Pinning failed
            completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
        }
        
    }
    
    func importPublicKeyReferenceFromDERCertificate(certData: NSData) -> SecKey? {
        guard let certRef = SecCertificateCreateWithData(nil, certData) else { return nil }

        var secTrust: SecTrust?
        let secTrustStatus = SecTrustCreateWithCertificates(certRef, nil, &secTrust)
        if secTrustStatus != errSecSuccess { return nil }

        var resultType: SecTrustResultType = SecTrustResultType.deny // ignore results.
        let evaluateStatus = SecTrustEvaluate(secTrust!, &resultType)
        if evaluateStatus != errSecSuccess { return nil }

        let publicKeyRef = SecTrustCopyPublicKey(secTrust!)
        return publicKeyRef
    }

}
