//
//  BaseRequestResult.swift
//  Pods
//
//  Created by Anderson Lucas C. Ramos on 05/07/17.
//
//

import Foundation

public struct BaseRequestResponse {
    internal(set) var statusCode: Int = 0
    internal(set) var responseData: Data?
}
