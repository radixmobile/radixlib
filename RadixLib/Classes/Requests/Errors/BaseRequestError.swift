//
//  LoginError.swift
//  RadixLib
//
//  Created by Anderson Lucas C. Ramos on 07/02/17.
//  Copyright © 2017 Radix Eng & Soft. All rights reserved.
//

import Foundation

public enum BaseRequestError {
	case statusCode(code: Int, data: Data?)
	
	case unsuppliedUrl
	
    case wrongRequestMethod(configuredMethod: BaseRequestMethod)
    
	case noResponseData
	
	case connection(error: Error)
}
