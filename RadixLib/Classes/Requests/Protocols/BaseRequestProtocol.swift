//
//  BaseRequestProtocol.swift
//  Pods
//
//  Created by Anderson Lucas C. Ramos on 13/06/17.
//
//

import Foundation

public protocol BaseRequestProtocol : class {
    func requestDidFinish(withSuccess response: BaseRequestResponse)
    func requestDidFinish(withError error: BaseRequestError)
}
