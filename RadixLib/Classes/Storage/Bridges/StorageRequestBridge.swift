//
//  StorageRequestBridge.swift
//  Pods
//
//  Created by Anderson Lucas C. Ramos on 05/07/17.
//
//

import Foundation

class StorageRequestBridge : BaseRequestProtocol, StorageExecuterProtocol {
    fileprivate var request: BaseRequest!
    
    var contentProvider: StorageContentProviderProtocol?
    
    weak var storageDelegate: StorageResultProtocol?
    
    var isRunning: Bool {
        return self.request.isRunning
    }
    
    var isCancelable: Bool {
        return self.request.isCancelable
    }
    
    init(with request: BaseRequest) {
        self.request = request
        self.request.delegate = self
    }
    
    // MARK: StorageFetcherProtocol implementations
    
    func fetch() {
        self.request.fetch()
    }
    
    func retain() {
        self.request.retain()
    }
    
    func update() {
        self.request.update()
    }
    
    func delete() {
        self.request.delete()
    }
    
    func cancel() {
        self.request.cancel()
    }
    
    // MARK: BaseRequestProtocol implementations
    
    func requestDidFinish(withSuccess response: BaseRequestResponse) {
        if let provider = self.contentProvider {
            let model = provider.objectModel(for: [
                "statusCode": response.statusCode,
                "responseData": response.responseData as Any
            ])
            self.storageDelegate?.storageResultDidFetch(model as Any)
        } else {
            self.storageDelegate?.storageResultDidFetch(response)
        }
    }
    
    func requestDidFinish(withError error: BaseRequestError) {
        self.storageDelegate?.storageResultDidFail(with: error)
    }
}
