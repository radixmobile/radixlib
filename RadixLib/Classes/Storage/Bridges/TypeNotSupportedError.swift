//
//  TypeNotSupportedError.swift
//  Pods
//
//  Created by Anderson Lucas C. Ramos on 05/07/17.
//
//

import Foundation

class TypeNotSupportedError : NSError {
    init(type: Any.Type) {
        super.init(domain: "Type \(type) not yet supported", code: -1, userInfo: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
