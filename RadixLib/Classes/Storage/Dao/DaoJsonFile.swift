//
//  DaoJsonFile.swift
//  RadixLib
//
//  Created by Anderson Lucas C. Ramos on 07/02/17.
//  Copyright © 2017 Radix Eng & Soft. All rights reserved.
//

import Foundation

public class DaoJsonFile: DataStorageProtocol, Logger {
	public static var instance: DataStorageProtocol = DaoJsonFile()

	fileprivate var jsonObject: [String: AnyObject] = [String: AnyObject]()
	
	fileprivate init() {
		self.loadJsonFromFile()
	}
	
	public func save<T>(_ object: T?, forKey key: String) {
		self.jsonObject[key] = object as AnyObject
	}
	
	public func load<T>(objectForKey key: String) -> T? {
		return self.jsonObject[key] as? T
	}
	
	public func delete(objectForKey key: String) {
		self.jsonObject.removeValue(forKey: key)
	}
	
	public func synchronize() {
		self.saveJsonToFile()
	}
	
	fileprivate func saveJsonToFile() {
		guard let path = self.getJsonFileName() else {
			log("could not get json file name in DaoJsonFile class, for saving")
			return
		}
		self.saveJson(withFilePath: path)
	}
	
	fileprivate func loadJsonFromFile() {
		guard let path = self.getJsonFileName() else {
			log("could not get json file name in DaoJsonFile class, for loading")
			return
		}
		self.loadJson(withFilePath: path)
	}
	
	fileprivate func getJsonFileName() -> String? {
		let paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)
		if paths.count > 0 {
			let cachesDir = paths[0]
			return NSString(string: cachesDir).appendingPathComponent("data.json")
		}
		return nil
	}
	
	fileprivate func loadJson(withFilePath path: String) {
		guard let jsonData = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
			log("could not load json data, probably the file does not exists yet.")
			return
		}
		guard let parsedJsonObject = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) else {
			log("could not parse json object, something is wrong")
			return
		}
		self.jsonObject = parsedJsonObject as! [String: AnyObject]
	}
	
	fileprivate func saveJson(withFilePath path: String) {
		do {
			let data = try JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
			NSData(data: data).write(toFile: path, atomically: true)
		} catch {
			log("could not save json file in DaoJsonFile class")
		}
	}
}
