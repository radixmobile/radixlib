//
//  DaoKeychain.swift
//  RadixLib
//
//  Created by Anderson Lucas C. Ramos on 08/02/17.
//  Copyright © 2017 Radix Eng & Soft. All rights reserved.
//

import Foundation
import KeychainAccess

internal struct DaoExceptionName {
	static let objectTypeNotSupported: NSExceptionName = NSExceptionName("objectTypeNotSupported")
}

internal class DaoKeychainException : NSException {
	init() {
		super.init(name: DaoExceptionName.objectTypeNotSupported, reason: "Cannot store this object type. Please, use Int, Float or Data types.", userInfo: nil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
}

public class DaoKeychain: DataStorageProtocol {
	public static var instance: DataStorageProtocol = DaoKeychain()
	
	fileprivate let keychain = Keychain()
	
	fileprivate init() {
		
	}
	
	public func save<T>(_ object: T?, forKey key: String) {
		if T.self is Data.Type {
			self.keychain[data: key] = object as? Data
		} else {
			self.keychain[key] = object as? String
		}
	}

	public func load<T>(objectForKey key: String) -> T? {
		if T.self is Data.Type {
			return self.keychain[data: key] as? T
		}
		return self.keychain[key] as? T
	}
	
	public func delete(objectForKey key: String) {
		try? self.keychain.remove(key)
	}
	
	public func synchronize() {
		// Do nothing for keychain
	}
}
