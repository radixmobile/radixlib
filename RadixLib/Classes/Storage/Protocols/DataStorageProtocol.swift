//
//  DataAccessProtocol.swift
//  RadixLib
//
//  Created by Anderson Lucas C. Ramos on 07/02/17.
//  Copyright © 2017 Radix Eng & Soft. All rights reserved.
//

import Foundation

public protocol DataStorageProtocol : class {
	static var instance: DataStorageProtocol { get }
	
	func save<T>(_ object: T?, forKey key: String)
	func load<T>(objectForKey key: String) -> T?
	func delete(objectForKey key: String)
	
	func synchronize()
}
