//
//  StorageCacheProtocol.swift
//  Pods
//
//  Created by Anderson Lucas C. Ramos on 04/07/17.
//
//

import Foundation

public protocol StorageCacheProtocol : class {
    func parseModelIntoData(_ model: Any) -> Data
    func parseDataIntoModel(_ data: Data) -> Any
}
