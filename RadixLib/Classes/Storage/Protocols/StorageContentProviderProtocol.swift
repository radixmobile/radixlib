//
//  StorageContentProviderProtocol.swift
//  Pods
//
//  Created by Anderson Lucas C. Ramos on 05/07/17.
//
//

import Foundation

public protocol StorageContentProviderProtocol : class {
    /// Provides a dictionary of keys and values that can be parsed into a ObjectModel
    func objectModel(for info: Dictionary<String, Any>) -> AnyObject?
    
    func data(for objectMode: Any) -> Data?
}
