//
//  StorageFetcherProtocol.swift
//  Pods
//
//  Created by Anderson Lucas C. Ramos on 04/07/17.
//
//

import Foundation

protocol StorageExecuterProtocol : StorageProviderProtocol {
    var storageDelegate: StorageResultProtocol? { get set }
    var contentProvider: StorageContentProviderProtocol? { get set }
}
