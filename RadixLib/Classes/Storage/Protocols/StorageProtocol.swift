//
//  StorageProtocol.swift
//  Pods
//
//  Created by Anderson Lucas C. Ramos on 30/05/17.
//
//

import Foundation

public protocol StorageProtocol : class {
    func storage(_ storage: Storage, didFinishWith result: Any)
    
    func storage<ErrorType>(_ storage: Storage, didFinishWith error: ErrorType)
}
