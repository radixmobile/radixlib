//
//  StorageProviderProtocol.swift
//  Pods
//
//  Created by Anderson Lucas C. Ramos on 05/07/17.
//
//

import Foundation

public protocol StorageProviderProtocol : class {
    var isRunning: Bool { get }
    var isCancelable: Bool { get }
    
    func fetch()
    func retain()
    func update()
    func delete()
    func cancel()
}
