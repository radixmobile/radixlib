//
//  StorageResultProtocol.swift
//  Pods
//
//  Created by Anderson Lucas C. Ramos on 04/07/17.
//
//

import Foundation

public protocol StorageResultProtocol : class {
    func storageResultDidFetch(_ result: Any)
    func storageResultDidFail<ErrorType>(with error: ErrorType)
}
