//
//  Storage.swift
//  Pods
//
//  Created by Anderson Lucas C. Ramos on 30/05/17.
//
//

import Foundation
import Swift_Json

public class Storage: NSObject, StorageResultProtocol {
	fileprivate var cacheStorage: (storage: DataStorage, key: String)!
	fileprivate var executingStorage: StorageExecuterProtocol!
	
	fileprivate weak var modelProtocol: StorageCacheProtocol?
	
	public weak var delegate: StorageProtocol?
	
    public override init() {
        super.init()
    }
    
	public func setup(_ storageType: DataStorageProtocol.Type, forKey key: String) {
		self.cacheStorage = (storage: DataStorage(withDaoType: storageType), key: key)
	}
	
	public func setup(_ modelProtocol: StorageCacheProtocol?) {
		self.modelProtocol = modelProtocol
	}
    
    fileprivate func commonStorageSetup(_ storage: StorageProviderProtocol, _ provider: StorageContentProviderProtocol? = nil) throws -> StorageExecuterProtocol {
        let bridge = try StorageBridgeFactory.createInstance(from: storage)
        bridge.storageDelegate = self
        bridge.contentProvider = provider
        return bridge
    }
	
    public func fetch(from storage: StorageProviderProtocol, with provider: StorageContentProviderProtocol? = nil) throws {
        self.executingStorage = try self.commonStorageSetup(storage, provider)
		self.executingStorage.fetch()
	}
    
    public func retain(from storage: StorageProviderProtocol, with provider: StorageContentProviderProtocol? = nil) throws {
        self.executingStorage = try self.commonStorageSetup(storage, provider)
        self.executingStorage.retain()
    }
    
    public func update(from storage: StorageProviderProtocol, with provider: StorageContentProviderProtocol? = nil) throws {
        self.executingStorage = try self.commonStorageSetup(storage, provider)
        self.executingStorage.update()
    }
    
    public func delete(from storage: StorageProviderProtocol, with provider: StorageContentProviderProtocol? = nil) throws {
        self.executingStorage = try self.commonStorageSetup(storage, provider)
        self.executingStorage.delete()
    }
	
	public func load() -> Any? {
		if let cache = self.cacheStorage {
			if let data: Data = cache.storage.load(objectForKey: cache.key) {
				let model: Any? = self.modelProtocol?.parseDataIntoModel(data)
				return model
			}
		}
		return nil
	}
	
	public func cancel() {
		guard let executing = self.executingStorage, executing.isCancelable else { return }
		executing.cancel()
	}
	
	// MARK: StorageResultProtocol implementations
	
	public func storageResultDidFetch(_ result: Any) {
		if let cache = self.cacheStorage {
			let data = self.modelProtocol?.parseModelIntoData(result)
			cache.storage.save(object: data, forKey: cache.key)
			cache.storage.synchronize()
		}
		
		self.delegate?.storage(self, didFinishWith: result)
	}
	
	public func storageResultDidFail<ErrorType>(with error: ErrorType) {
		self.delegate?.storage(self, didFinishWith: error)
	}
}
