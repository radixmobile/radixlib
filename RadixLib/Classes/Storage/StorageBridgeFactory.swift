//
//  StorageBridgeFactory.swift
//  Pods
//
//  Created by Anderson Lucas C. Ramos on 05/07/17.
//
//

import Foundation

internal class StorageBridgeFactory {
    class func createInstance(from: StorageProviderProtocol) throws -> StorageExecuterProtocol {
        if from is BaseRequest {
            return StorageRequestBridge(with: from as! BaseRequest)
        }
        throw TypeNotSupportedError(type: type(of: from))
    }
}
