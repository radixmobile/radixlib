import UIKit

public protocol FloatingDatePickerDelegate: NSObjectProtocol {
	func floatingDatePickerDidSelect(date: Date)
}


//DatePicker Mode
public enum FloatingDatePickerType {
	case date, time, countdown
	public func dateType() -> UIDatePicker.Mode {
		switch self {
		case .date: return .date
		case .time: return .dateAndTime
		case .countdown: return .countDownTimer
		}
	}
}

open class FloatingDatePicker: UIView, UIGestureRecognizerDelegate {
	//Delegate
	open weak var delegate: FloatingDatePickerDelegate?
	
	//Properties
	fileprivate weak var contentView: UIView!
	fileprivate var containerView: UIView!
	fileprivate var backgroundView: UIView!
	fileprivate var datePickerView: UIDatePicker!
	
	//Custom Properties
	open var showBlur = true // Default Yes
	open var datePickerType = FloatingDatePickerType.date // Default .date
	open var tapToDismiss = true // Default Yes
	open var buttonText = "Select" // Default "Select"
	open var buttonFontColor = UIColor.blue // Default Blue
	open var buttonColor = UIColor.clear // Default Clear
	open var showShadow = true // Optional
	open var showCornerRadius = true // Optional
	
	open var datePickerStartDate = Date() { // Optional
		didSet {
			if let picker = self.datePickerView {
				picker.date = self.datePickerStartDate
			}
		}
	}
	open var datePickerMinimumDate = Date() { // Optional
		didSet {
			if let picker = self.datePickerView {
				picker.minimumDate = self.datePickerMinimumDate
			}
		}
	}
	open var datePickerMaximumDate = Date() { // Optional
		didSet {
			if let picker = self.datePickerView {
				picker.maximumDate = self.datePickerMaximumDate
			}
		}
	}
	
	open fileprivate(set) weak var dateTextField: UITextField?
	open fileprivate(set) var dateFormatter: DateFormatter!
	
	public init() {
		super.init(frame: CGRect.zero)
		self.backgroundColor = UIColor.clear
	}
	
	deinit {
		self.containerView = nil
		self.backgroundView = nil
		self.datePickerView = nil
	}
	
	open func show(attachToView view: UIView) {
		self.show(view)
	}
	
	open func set(textField: UITextField?, withDateFormatter dateFormatter: DateFormatter) {
		self.dateTextField = textField
		self.dateFormatter = textField != nil ? dateFormatter : nil
	}
	
	//Show View
	fileprivate func show(_ inView: UIView) {
		self.contentView = inView
		
		self.containerView = UIView()
		self.containerView.frame = CGRect(x: 0, y: 0, width: inView.bounds.width, height: inView.bounds.height)
		self.containerView.backgroundColor = UIColor.clear
		self.containerView.alpha = 0
		
		self.contentView.addSubview(self.containerView)
		
		if showBlur {
			_showBlur()
		}
		
		self.backgroundView = createBackgroundView()
		self.containerView.addSubview(self.backgroundView)
		
		self.datePickerView = createDatePicker()
		self.backgroundView.addSubview(self.datePickerView)
		
		//Round .Left / .Right Corners of DatePicker View
		if showCornerRadius {
			let path = UIBezierPath(roundedRect:self.datePickerView.bounds, byRoundingCorners:[.topRight, .topLeft], cornerRadii: CGSize(width: 10, height: 10))
			let maskLayer = CAShapeLayer()
			
			maskLayer.path = path.cgPath
			self.datePickerView.layer.mask = maskLayer
		}
		
		self.backgroundView.addSubview(self.addSelectButton())
		
		
		//Show UI Views
		UIView.animate(withDuration: 0.15, animations: { [weak self] in
			self?.containerView.alpha = 1
		}, completion: { [weak self] (success:Bool) in
			UIView.animate(withDuration: 0.30, delay: 0, options: .transitionCrossDissolve, animations: { [weak self] in
				guard let this = self else { return }
				self?.backgroundView.frame.origin.y = this.containerView.bounds.height / 2 - 125
			}, completion: { (success:Bool) in
				
			})
		})
		
		if tapToDismiss {
			let tap = UITapGestureRecognizer(target: self, action: #selector(FloatingDatePicker.dismiss(_:)))
			tap.delegate = self
			self.containerView.addGestureRecognizer(tap)
		}
		
		self.layoutSubviews()
	}
	
	//Handle Tap Dismiss
	@objc func dismiss(_ sender: UITapGestureRecognizer? = nil) {
		UIView.animate(withDuration: 0.15, animations: { [weak self] in
			guard let this = self else { return }
			this.backgroundView.frame.origin.y += this.containerView.bounds.maxY
		}, completion: { [weak self] (success:Bool) in
			UIView.animate(withDuration: 0.05, delay: 0, options: .transitionCrossDissolve, animations: { [weak self] in
				self?.containerView.alpha = 0
			}, completion: { [weak self] (success:Bool) in
				guard let this = self else { return }
				this.containerView.removeGestureRecognizer(UIGestureRecognizer(target: this, action: #selector(FloatingDatePicker.dismiss(_:))))
				self?.containerView.removeFromSuperview()
				self?.removeFromSuperview()
			})
		})
	}
	
	//Show Blur Effect
	fileprivate func _showBlur() {
		let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
		let blurEffectView = UIVisualEffectView(effect: blurEffect)
		blurEffectView.frame = self.contentView.bounds
		blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
		self.containerView.addSubview(blurEffectView)
	}
	
	//Create DatePicker
	fileprivate func createDatePicker() -> UIDatePicker {
		let datePickerView: UIDatePicker = UIDatePicker(frame: CGRect(x: 0, y: -60, width: self.backgroundView.bounds.width, height: self.backgroundView.bounds.height))
		datePickerView.autoresizingMask = [.flexibleWidth]
		datePickerView.clipsToBounds = true
		datePickerView.backgroundColor = UIColor.white
		datePickerView.datePickerMode = self.datePickerType.dateType()
		datePickerView.date = self.datePickerStartDate
		datePickerView.minimumDate = self.datePickerMinimumDate
		datePickerView.maximumDate = self.datePickerMaximumDate
		return datePickerView
	}
	//
	//Create Background Container View
	fileprivate func createBackgroundView() -> UIView {
		let bgView = UIView(frame: CGRect(x: self.containerView.frame.width / 2 - 150, y: self.containerView.bounds.maxY + 100, width: 300, height: 320))
		bgView.autoresizingMask = [.flexibleWidth]
		bgView.backgroundColor = UIColor.clear
		
		if showShadow {
			bgView.layer.shadowOffset = CGSize(width: 3, height: 3)
			bgView.layer.shadowOpacity = 0.7
			bgView.layer.shadowRadius = 2
		}
		if showCornerRadius {
			bgView.layer.cornerRadius = 10.0
		}
		return bgView
	}
	
	fileprivate func addSelectButton() -> UIButton {
		let btn = UIButton(type: .system)
		btn.frame = CGRect(x: self.backgroundView.frame.width / 2 - 150, y: self.datePickerView.frame.maxY, width: self.backgroundView.frame.size.width, height: 48)
		btn.setTitle(self.buttonText, for: UIControl.State())
		btn.titleLabel?.font = UIFont.systemFont(ofSize: 20)
		btn.tintColor = self.buttonFontColor
		btn.backgroundColor = self.buttonColor
		btn.addTarget(self, action: #selector(FloatingDatePicker.didSelectDate(_:)), for: .touchUpInside)
		
		//Round .Left / .Right Corners of DatePicker View
		if showCornerRadius {
			let path = UIBezierPath(roundedRect: btn.bounds, byRoundingCorners:[.bottomRight, .bottomLeft], cornerRadii: CGSize(width: 10, height: 10))
			let maskLayer = CAShapeLayer()
			
			maskLayer.path = path.cgPath
			btn.layer.mask = maskLayer
		}
		
		return btn
	}
	
	@objc fileprivate func didSelectDate(_ sender: UIButton) {
		if self.dateTextField != nil {
			let dateString = self.dateFormatter.string(from: self.datePickerView.date)
			self.dateTextField?.text = dateString
			self.dismiss()
		}
		if delegate != nil {
			self.delegate?.floatingDatePickerDidSelect(date: self.datePickerView.date)
			self.dismiss()
		}
	}
	
	required public init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
